# Application dashboard

Simple graphical user interface to install, update, run & configure git & pip applications.  
(*Currently limited to python applications*)

## Installation

**Ubuntu/Debian**:
```bash
# Dependencies
sudo apt update && sudo apt install git python3 python3-tk python3-pip
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
# Download and install
git clone https://gitlab.com/panasiam_clothing/app-dashboard.git
cd app-dashboard
poetry build
python3 -m pip install --upgrade pip && python3 -m pip install .
```

**Windows**:
- Download & install a recent version of Python from [here](https://www.python.org/downloads/windows/)
- Download & install a recent version of Git from [here](https://git-scm.com/download/win)
- Clone the project and install:
```bash
git clone https://gitlab.com/panasiam_clothing/app-dashboard.git
cd app-dashboard
python -m pip install --upgrade pip && python -m pip install poetry
poetry build
python -m pip install .
```

## Configuration

To manage an application through the dashboard, the dashboard requires a configuration that describes specific paths, names or parameter configurations.
Example:
```json
{
    "applications": [
        {
            "name": "Example application",
            "type": "git",
            "url": "https://gitlab.com/example_user/example_app.git",
            "branch": "master",
            "remote": "origin",
            "local_path": "/home/example_user/Documents/example_app",
            "pip_name": "example_app",
            "configuration_path": "/home/example_user/.config/example_app/config.json"
        }
    ]
}
```

There are currently two types of applications, **git** (Fetch & pull updates directly from the repository) and **pip** (Search for new official releases). A **git** application requires metadata to identify where to pull and where to place the data (`url`, `branch`, `remote`, `local_path`). The program is installed on the system via the pip command, so both types need the `pip_name` argument. The `name` argument contains the name displayed within the graphical user interface.

Additionally, it is possible to configure the start parameters for an application via the `default_start_parameter` argument. To ease the process of modifying the parameters of an application it is possible to define a individual configuration for each parameter within the `parameter_configuration` array (visible within the detailed view of an application).
**Example**:
```json
{
    "applications": [
        {
            "name": "Example application",
            "type": "pip",
            "pip_name": "example_app",
            "configuration_path": "/home/example_user/.config/example_app/config.json",
            "default_start_parameter": "--config /etc/example_config.json --log /tmp/example.log",
            "parameter_configuration": [
                {
                    "parameter_name": "config",
                    "display_name": "Configuration file",
                    "type": "path"
                },
                {
                    "parameter_name": "log",
                    "display_name": "Logging destination file",
                    "type": "path"
                }
            ]
        }
    ]
}
```

## Notes

The button icons are hosted on imgur: https://imgur.com/a/2PEEZCS and will be cached on first use.
