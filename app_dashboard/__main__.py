"""
Convinient graphical user interface for managing multiple applications.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import json
import os
import pathlib
import shlex
import sys
import tkinter
import tkinter.ttk
import subprocess
from typing import Callable, Dict, List, TypedDict, Union
import jsonschema
from loguru import logger
from PIL import Image, ImageTk
import platformdirs
from pyshortcuts import get_desktop
import requests
import validators


from app_dashboard.config_schema import updater_configuration_schema


# Dimensions
WINDOW_WIDTH = 700
WINDOW_HEIGHT = 700
DETAILED_HEADER = ('Helvitica', 23, 'bold')
SQUARE_FONT = ('Helvitica', 15)
VERSION_FONT = ('Helvitica', 8)
SQUARE_SIZE = 203
SQUARE_PADDING = 23
MINI_BUTTON_SIZE = 30

# Color palette
FRAME_BACKGROUND = '#f4cccc'
SQUARE_BACKGROUND = '#fffaed'
ACTIVE_SQUARE_BACKGROUND = '#cfe2f3'
TEXT_COLOR = '#000000'
ERROR_SQUARE_BACKGROUND = '#ffe5a1'
ERROR_ACTIVE_SQUARE_BACKGROUND = '#ffb6a1'


def python_executable() -> str:
    """
    The python executable is used at multiple points in the application to call
    a python module within a subprocess. Subprocess is not able to handle these
    commands when the path to the executable contains spaces, use only the name
    in those case.
    """
    system_python = pathlib.Path(sys.executable)
    if str(system_python).count(' '):
        return system_python.name
    return str(system_python)


def get_editor() -> List[str]:
    if os.name == 'nt':
        return ['notepad']
    elif os.name == 'posix':
        return ['gedit', 'kate', 'vim', 'xdg-open']
    else:
        raise RuntimeError(f'Unsupported operating system {os.name}')


def open_configuration_in_editor(path: pathlib.Path) -> None:
    for editor in get_editor():
        try:
            subprocess.run(f'{editor} {path}', shell=True, check=True)
            break
        except subprocess.CalledProcessError:
            logger.warning(f'Opening configuration with {editor} failed.')
            continue


def install_from_current_directory(path: pathlib.Path) -> bool:
    try:
        subprocess.run(
            f'{python_executable()} -m pip install . --user',
            shell=True, check=True, cwd=path
        )
    except subprocess.CalledProcessError:
        try:
            subprocess.run(
                f'{python_executable()} -m pip install .',
                shell=True, check=True, cwd=path
            )
        except subprocess.CalledProcessError:
            return False
    return True


class ConfigHandler:
    def __init__(self):
        folder = pathlib.Path(
            platformdirs.user_config_dir('app_dashboard')
        )
        self.path = folder / 'config.json'
        try:
            self.config = self.read_configuration()
        except json.decoder.JSONDecodeError as err:
            raise RuntimeError(
                f'Invalid app_dashboard configuration JSON at {self.path}'
            ) from err
        if not self.config:
            raise RuntimeError(
                'app_dashboard configuration is empty, please fill '
                f'the file found at {self.path}'
            )
        jsonschema.validate(
            instance=self.config, schema=updater_configuration_schema
        )

    def read_configuration(self) -> dict:
        if not self.path.stat().st_size:
            return {}
        with self.path.open(mode='r', encoding='utf-8') as config_file:
            return json.load(config_file)

    def write_configuration(self) -> None:
        with open(self.path, mode='w', encoding='utf-8') as config_file:
            config_file.write(json.dumps(self.config, indent=4))


class Interface:
    def __init__(self, config_handler: ConfigHandler) -> None:
        self.root = tkinter.Tk()
        self.root.wm_title('Application Dashboard')
        self.root.geometry(f'{WINDOW_WIDTH}x{WINDOW_HEIGHT}')
        self.root.resizable(width=False, height=False)

        self.container = tkinter.Frame(self.root, background=FRAME_BACKGROUND)
        self.container.place(
            x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT
        )
        self.cache_dir = pathlib.Path(
            platformdirs.user_cache_dir(appname='app_dashboard')
        )
        self.cache_dir.mkdir(parents=True, exist_ok=True)
        self.config_handler = config_handler
        self.config = config_handler.config
        self.__update_pip()
        if len(
            [
                app for app in self.config['applications']
                if app['type'] == 'pip'
            ]
        ) > 0:
            self.outdated_apps = self.__get_outdated_apps()
        else:
            self.outdated_apps = []
        self.applications = {}
        self.current_square = tkinter.StringVar()
        self.current_square.trace('w', self.__highlight_selection)
        column_index = 0
        row_index = 0
        if len(self.config['applications']) > 9:
            print(
                'ERROR the current design settings are set for a maximum of 9'
                ' Applications'
            )
            sys.exit(1)
        for app in self.config['applications']:
            app_container = AppSquare(parent=self, config=app)
            col_padding = column_index * SQUARE_PADDING
            row_padding = row_index * SQUARE_PADDING
            app_container.place(
                x=SQUARE_PADDING + column_index * SQUARE_SIZE + col_padding,
                y=SQUARE_PADDING + row_index * SQUARE_SIZE + row_padding,
                width=SQUARE_SIZE, height=SQUARE_SIZE
            )
            self.applications.update({app['name']: app_container})
            column_index += 1
            if (column_index + 1) % 4 == 0:
                column_index = 0
                row_index += 1
        self.current_square.set(list(self.applications.keys())[0])
        self.root.bind_all('<Left>', self.__select_previous_square)
        self.root.bind_all('<Right>', self.__select_next_square)
        self.root.bind_all('<Return>', self.__open_app_details)
        self.menubar = tkinter.Menu(
            self.root, background=FRAME_BACKGROUND,
            activebackground=ACTIVE_SQUARE_BACKGROUND,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.menubar.add_command(
            label='Edit configuration', command=self.__edit_configuration
        )
        self.menubar.add_command(
            label='Update Application dashboard', command=self.__update_self
        )
        self.root.configure(menu=self.menubar)

    def __edit_configuration(self) -> None:
        path = pathlib.Path(
            platformdirs.user_config_dir(appname='app_dashboard')
        ) / 'config.json'
        open_configuration_in_editor(path=path)


    def __update_self(self) -> None:
        local_path = pathlib.Path(self.config['install_location'])
        subprocess.run(
            'git fetch && git pull && poetry build', shell=True,
            check=True, cwd=local_path
        )
        install_from_current_directory(path=local_path)

    def __select_next_square(self, event) -> None:
        del event
        current_index = list(self.applications).index(
            self.current_square.get()
        )
        if current_index == len(self.applications) - 1:
            next_index = 0
        else:
            next_index = current_index + 1
        next_square = list(self.applications)[next_index]
        self.current_square.set(next_square)

    def __select_previous_square(self, event) -> None:
        del event
        current_index = list(self.applications).index(
            self.current_square.get()
        )
        if current_index == 0:
            next_index = len(self.applications) - 1
        else:
            next_index = current_index - 1
        next_square = list(self.applications)[next_index]
        self.current_square.set(next_square)

    def __open_app_details(self, event) -> None:
        del event
        app = self.applications[self.current_square.get()]
        if app.current_version != 'not found':
            app.show_details()
        else:
            app.install()

    def __highlight_selection(self, *args) -> None:
        del args
        for name, app in self.applications.items():
            if name == self.current_square.get():
                app.configure(borderwidth=3)
            else:
                app.configure(borderwidth=1)

    @staticmethod
    def __update_pip() -> None:
        subprocess.run(
            f'{python_executable()} -m pip install --upgrade pip',
            shell=True, check=True
        )

    @staticmethod
    def __get_outdated_apps() -> list:
        pip_list_command = 'pip list -o'
        if os.name != 'nt':
            pip_list_command = shlex.split(pip_list_command)
        return subprocess.check_output(
            pip_list_command, universal_newlines=True
        ).split('\n')


class ScrollableFrame(tkinter.Frame):
    """
    Implementation of a hacky workaround to Tkinters inabillity to apply a
    scrollbar to a regular frame widget.
    The workaround involves adding a canvas to the frame and adding a frame
    widget to the canvas as window.
    One drawback that I discovered so far, is that I cannot add another frame
    to `self.frame` instead I have to place widgets like labels and entry boxes
    directly to it.
    """
    def __init__(self, parent: tkinter.Frame) -> None:
        tkinter.Frame.__init__(
            self, parent, background=FRAME_BACKGROUND, border=0
        )
        self._canvas = tkinter.Canvas(
            self, borderwidth=0, background=FRAME_BACKGROUND,
            highlightthickness=0, highlightbackground=FRAME_BACKGROUND, bd=0
        )
        self.frame = tkinter.Frame(
            self._canvas, background=FRAME_BACKGROUND, bd=0
        )
        self._vsb = tkinter.ttk.Scrollbar(
            self, orient='vertical', command=self._canvas.yview
        )
        self._canvas.configure(yscrollcommand=self._vsb.set)
        self._vsb.pack(side="right", fill="y")
        self._canvas.pack(side="left", fill="both", expand=True)
        self._canvas.create_window(
            (4, 4), window=self.frame, anchor='nw',
            tags='self.frame'
        )

        self.frame.bind("<Configure>", self.__on_frame_configure)
        self.frame.bind("<Enter>", self.__bound_to_mousewheel)
        self.frame.bind("<Leave>", self.__unbound_to_mousewheel)

    def __bound_to_mousewheel(self, event):
        del event
        if os.name == 'posix':
            self._canvas.bind_all("<Button-4>", self.__on_mousewheel)
            self._canvas.bind_all("<Button-5>", self.__on_mousewheel)
        else:
            self._canvas.bind_all("<MouseWheel>", self.__on_mousewheel)

    def __unbound_to_mousewheel(self, event):
        del event
        if os.name == 'posix':
            self._canvas.unbind_all("<Button-4>")
            self._canvas.unbind_all("<Button-5>")
        else:
            self._canvas.unbind_all("<MouseWheel>")

    def __on_mousewheel(self, event):
        scroll_value = 0
        if event.num == 5 or event.delta == -120:
            scroll_value = 1
        elif event.num == 4 or event.delta == 120:
            scroll_value = -1
        self._canvas.yview_scroll(scroll_value, "units")

    def __on_frame_configure(self, event):
        """Reset the scroll region to encompass the inner frame"""
        del event
        self._canvas.configure(scrollregion=self._canvas.bbox("all"))


def get_parameter_from_string(string: str) -> dict:
    parameter = {}
    while 1:
        parameter_name = ''
        double_dash = string.find('--')
        if double_dash != 0:
            single_dash = string.find('-')
            if single_dash > 0:
                # Every option after the first should have a space in front
                option_single_dash = string.find(' -')
                option_double_dash = string.find(' --')
                if option_single_dash == -1 and option_double_dash == -1:
                    break
                if option_single_dash == -1 or option_double_dash == -1:
                    start = max(option_single_dash, option_double_dash)
                else:
                    start = min(option_single_dash, option_double_dash)
                string = string[start + 1:]
                continue
            if single_dash == 0:
                parameter_name = string[single_dash + 1:string.find(' ')]
            if single_dash != 0:
                break
        elif double_dash == 0:
            parameter_name = string[double_dash + 2:string.find(' ')]

        string = string[string.find(' '):]
        if string.find(' \"') == 0:
            # skip the inital space and the quotation
            value_start = string[2:]
            value = value_start[:value_start.find('\"')]
        else:
            next_space = string.find(' ', 1)
            if next_space > 0:
                value = string[1:next_space]
            else:
                value = string[1:]
        parameter.update({parameter_name: value})
    return parameter


def get_dashes(parameter: str) -> str:
    if len(parameter) == 1:
        dash = '-'
    else:
        dash = '--'
    return dash


def replace_parameter_in_string(
    parameter: str, new_value: str, string: str
) -> str:
    dash = get_dashes(parameter=parameter)
    position_parameter = string.find(f'{dash}{parameter}')
    if position_parameter == -1:
        return ''
    string_after_parameter = string[
        position_parameter + len(dash) + len(parameter):
    ]
    quotation = string_after_parameter.find(' \"')
    next_single_dash = string_after_parameter.find(' --')
    next_double_dash = string_after_parameter.find(' --')
    if next_single_dash == -1 ^ next_double_dash == -1:
        next_dash = max(next_single_dash, next_double_dash)
    else:
        next_dash = min(next_single_dash, next_double_dash)

    # Get the parameter in between two quotations if the quotations are found
    # before the next option starts.
    if (
        quotation != -1 and next_dash != -1 and quotation < next_dash or
        quotation != -1 and next_dash == -1
    ):
        value = string_after_parameter[
            1:string_after_parameter.find('\"', 2) + 1
        ]
    else:
        if string_after_parameter.find(' ', 1) != -1:
            value = string_after_parameter[
                1:string_after_parameter.find(' ', 1)
            ]
        else:
            value = string_after_parameter[1:]
    start_position = string.find(value)
    end_position = start_position + len(value)
    return string[:start_position] + new_value + string[end_position:]


def validate_parameter(value: str, parameter_type: str) -> bool:
    assert parameter_type in ['path', 'url', 'text']
    if parameter_type == 'path':
        return pathlib.Path(value).exists()
    if parameter_type == 'url':
        return validators.url(value)
    return True


class ParameterEditFrame(ScrollableFrame):
    def __init__(self, parent) -> None:
        ScrollableFrame.__init__(self, parent)
        self.parent = parent
        self.parameters = {}
        parameter_config = self.parent.parent.config['parameter_configuration']
        for row, parameter in enumerate(parameter_config):
            var = tkinter.StringVar()
            label = tkinter.Label(
                self.frame, text=parameter['display_name'],
                background=FRAME_BACKGROUND, foreground=TEXT_COLOR
            )
            entry = tkinter.Entry(
                self.frame, textvariable=var, background=SQUARE_BACKGROUND,
                foreground=TEXT_COLOR,
                width=round(WINDOW_WIDTH / 11.5)
            )
            self.parameters[parameter['parameter_name']] = {
                'variable': var, 'label': label, 'entry': entry,
                'type': parameter['type']
            }
            label.grid(row=row, column=0)
            entry.grid(row=row, column=1, columnspan=3)
        self.set()

    def get(self) -> dict:
        return {
            parameter: widgets['variable'].get()
            for parameter, widgets in self.parameters.items()
            if widgets['variable'].get()
        }

    def set(self) -> None:
        try:
            config = self.parent.parent.config['default_start_parameter']
        except KeyError:
            return
        parts = get_parameter_from_string(string=config)
        for parameter, value in parts.items():
            row = self.parameters[parameter]
            if not validate_parameter(value=value, parameter_type=row['type']):
                row['entry'].configure(background=ERROR_SQUARE_BACKGROUND)
            else:
                row['entry'].configure(background=SQUARE_BACKGROUND)
            row['variable'].set(value)

    def save(self) -> None:
        try:
            config = self.parent.parent.config['default_start_parameter']
        except KeyError:
            config_entry = ''
            # Set the default_start_parameter
            for parameter, value in self.get().items():
                if not config_entry:
                    config_entry += str(
                        f'{get_dashes(parameter=parameter)}{parameter} {value}'
                    )
                else:
                    config_entry += str(
                        f' {get_dashes(parameter=parameter)}{parameter} '
                        f'{value}'
                    )
            self.parent.parent.config['default_start_parameter'] = config_entry
            self.parent.parent.parent.config_handler.write_configuration()
            return
        for parameter, value in self.get().items():
            row = self.parameters[parameter]
            if not validate_parameter(value=value, parameter_type=row['type']):
                row['entry'].configure(background=ERROR_SQUARE_BACKGROUND)
                continue
            row['entry'].configure(background=SQUARE_BACKGROUND)
            if value.find(' ') != -1:
                value = f'\"{value}"'
            adjusted_config = replace_parameter_in_string(
                parameter=parameter, new_value=value, string=config
            )
            if adjusted_config:
                config = adjusted_config
            else:
                new_parameter = str(
                    f' {get_dashes(parameter=parameter)}{parameter} {value}'
                )
                config += new_parameter
        self.parent.parent.config['default_start_parameter'] = config
        self.parent.parent.parent.config_handler.write_configuration()


class DetailedView(tkinter.Frame):
    def __init__(self, parent) -> None:
        tkinter.Frame.__init__(
            self, parent.parent.container, background=FRAME_BACKGROUND
        )
        self.parent = parent
        self.custom_parameter = None
        self.parameter_frame = None
        self.name = tkinter.Label(
            self, text=f"{parent.config['name']}", font=DETAILED_HEADER,
            justify=tkinter.CENTER, background=FRAME_BACKGROUND,
            foreground=TEXT_COLOR
        )
        self.current_version_label = tkinter.Label(
            self, text=self.parent.current_version,
            background=FRAME_BACKGROUND, foreground=TEXT_COLOR,
        )
        state = 'Up-to-date'
        if self.parent.update_available:
            state = 'Update available'
        self.update_available_label = tkinter.Label(
            self, text=state,
            background=FRAME_BACKGROUND, foreground=TEXT_COLOR
        )
        self.update_button = tkinter.Button(
            self, text='Update', command=self.parent.update,
            background=SQUARE_BACKGROUND, foreground=TEXT_COLOR,
            activebackground=ACTIVE_SQUARE_BACKGROUND
        )
        self.edit_config_button = tkinter.Button(
            self, text='Edit configuration', command=self.__edit_config,
            background=SQUARE_BACKGROUND, foreground=TEXT_COLOR,
            activebackground=ACTIVE_SQUARE_BACKGROUND
        )
        self.play_button = tkinter.Button(
            self, text='Play', command=self.__play,
            background=SQUARE_BACKGROUND, foreground=TEXT_COLOR,
            activebackground=ACTIVE_SQUARE_BACKGROUND
        )
        self.create_link_button = tkinter.Button(
            self, text='Create desktop link', command=self.__create_link,
            background=SQUARE_BACKGROUND, foreground=TEXT_COLOR,
            activebackground=ACTIVE_SQUARE_BACKGROUND
        )
        self.back_button = tkinter.Button(
            self, text='Back', command=self.__go_back,
            background=SQUARE_BACKGROUND, foreground=TEXT_COLOR,
            activebackground=ACTIVE_SQUARE_BACKGROUND
        )
        name_length = len(parent.config['name'])
        row_height = 45
        first_column = 50
        second_column = WINDOW_WIDTH - (WINDOW_WIDTH / 4) - first_column
        width = WINDOW_WIDTH / 4
        if 'parameter_configuration' not in self.parent.config:
            self.parameter_label = tkinter.Label(
                self, text='Custom parameter:',
                background=FRAME_BACKGROUND, foreground=TEXT_COLOR
            )
            self.custom_parameter = tkinter.StringVar()
            self.parameter = tkinter.Entry(
                self, textvariable=self.custom_parameter,
                background=SQUARE_BACKGROUND, foreground=TEXT_COLOR
            )
            self.parameter_label.place(
                x=first_column, y=row_height * 5, width=WINDOW_WIDTH / 5,
                height=row_height
            )
            self.parameter.place(
                x=WINDOW_WIDTH / 5 + 100, y=row_height * 5,
                width=WINDOW_WIDTH - (WINDOW_WIDTH / 5 + 100) - 50,
                height=row_height
            )
        else:
            self.parameter_frame = ParameterEditFrame(parent=self)
            self.parameter_frame.place(
                x=first_column, y=row_height * 5,
                width=WINDOW_WIDTH - first_column, height=3 * row_height
            )
            self.save_button = tkinter.Button(
                self, text='Save', command=self.parameter_frame.save,
                background=SQUARE_BACKGROUND, foreground=TEXT_COLOR,
                activebackground=ACTIVE_SQUARE_BACKGROUND
            )
            self.save_button.place(
                x=first_column, y=row_height * 9,
                width=width, height=row_height
            )
        self.name.place(
            x=(WINDOW_WIDTH / 2) - (name_length * (23 // 2)),
            y=0, width=name_length * 23, height=30
        )
        self.current_version_label.place(
            x=first_column, y=row_height, width=width, height=row_height
        )
        self.update_available_label.place(
            x=second_column, y=row_height, width=width, height=row_height
        )
        self.update_button.place(
            x=first_column, y=row_height * 3, width=width, height=row_height
        )
        self.edit_config_button.place(
            x=second_column, y=row_height * 3, width=width, height=row_height
        )
        self.play_button.place(
            x=first_column, y=row_height * 11, width=width, height=row_height
        )
        self.create_link_button.place(
            x=second_column, y=row_height * 11, width=width, height=row_height
        )
        self.back_button.place(
            x=WINDOW_WIDTH / 2 - width / 2, y=row_height * 13, width=width,
            height=row_height
        )
        self.bind_all('<Escape>', func=self.__go_back)

    def __play(self, *args) -> None:
        del args
        parameter = ''
        if self.custom_parameter:
            parameter = self.custom_parameter.get()
        self.parent.play(parameter=parameter)

    def __edit_config(self) -> None:
        path = pathlib.Path(self.parent.config['configuration_path'])
        open_configuration_in_editor(path=path)

    def __create_link(self) -> None:
        if self.custom_parameter and self.custom_parameter.get():
            parameter = f' {self.custom_parameter.get()}'
        else:
            try:
                parameter = f" {self.parent.config['default_start_parameter']}"
            except KeyError:
                parameter = ''
        name = self.parent.config['name'].replace(' ', '-')
        command = str(
            f" -m {self.parent.config['pip_name']}{parameter}"
        )
        if os.name == 'nt':
            # Instead of a link we use batch scripts for Windows because of the
            # length limitation to 256 characters for PATHS.
            link_path = pathlib.Path(get_desktop()) / f'{name}.bat'
            with open(link_path, mode='w', encoding='utf-8') as link_file:
                link_file.write(
                    f'@echo off\n{python_executable()}{command}\npause'
                )
            return
        if os.name == 'posix':
            # TODO add support for icons
            lines = [
                '[Desktop Entry]\n',
                f'Name={name}\n',
                'Type=Application\n',
                f'Comment={name} link\n',
                'Terminal=true\n',
                f'Exec={python_executable()}{command}\n'
            ]
            link_path = pathlib.Path(get_desktop()) / f"{name}.desktop"
            with open(link_path, 'w', encoding='utf-8') as link_file:
                link_file.writelines(lines)
        else:
            raise RuntimeError(
                f'Link creation not support on {os.name} systems.'
            )

    def __go_back(self, *args) -> None:
        del args
        self.destroy()


class IconButton(tkinter.Button):
    def __init__(
        self, parent, name: str, url: str, command: Callable[[], None]
    ) -> None:
        cache_dir = parent.parent.cache_dir
        file_path = cache_dir / f'{name}.png'
        if not file_path.exists():
            request = requests.get(url)
            request.raise_for_status()
            with open(file_path, mode='wb') as temp_file:
                temp_file.write(request.content)
        img = Image.open(file_path).resize(
            (MINI_BUTTON_SIZE, MINI_BUTTON_SIZE), Image.ANTIALIAS
        )
        self.image = ImageTk.PhotoImage(img)
        tkinter.Button.__init__(
            self, parent, image=self.image, command=command,
            relief=tkinter.FLAT, background=SQUARE_BACKGROUND,
            activebackground=SQUARE_BACKGROUND, borderwidth=0,
            border=0
        )
        self.parent = parent


class SquareElement(TypedDict):
    color: str
    active_color: str
    widget: Union[tkinter.Button, tkinter.Label]


class AppSquare(tkinter.Frame):
    def __init__(self, parent: Interface, config: dict) -> None:
        tkinter.Frame.__init__(
            self, parent.container, borderwidth=1, width=SQUARE_SIZE,
            height=SQUARE_SIZE, relief=tkinter.RAISED,
            background=SQUARE_BACKGROUND
        )
        self.parent = parent
        self.config = config
        self.update_available = False
        self.elements: Dict[str, SquareElement] = {}
        for i in range(0, 3):
            self.grid_rowconfigure(index=i, weight=1)
            self.grid_columnconfigure(index=i, weight=1)
        self.title = tkinter.Label(
            self, text=f"{config['name']}", font=SQUARE_FONT,
            wraplength=SQUARE_SIZE - 10, justify=tkinter.CENTER,
            background=SQUARE_BACKGROUND, foreground=TEXT_COLOR
        )
        self.elements.update(
            {
                'title': {
                    'widget': self.title,
                    'color': SQUARE_BACKGROUND,
                    'active_color': ACTIVE_SQUARE_BACKGROUND
                }
            }
        )
        self.title.grid(row=0, column=0, columnspan=3)
        self.__build_quick_access_interface()

    def __build_quick_access_interface(self) -> None:
        self.current_version = self.__get_current_version()
        if self.current_version != 'not found':
            local_path = pathlib.Path(self.config['local_path'])
            if not local_path.exists():
                raise RuntimeError(
                    'Folder to local source files has been removed, the source'
                    ' files are required at the configured location '
                    f'({local_path}) to update the application.'
                )
            self.__update_remote()
            self.current_version_label = tkinter.Label(
                self, text=f'Current version: {self.current_version}',
                background=SQUARE_BACKGROUND, foreground=TEXT_COLOR,
                font=VERSION_FONT
            )
            self.play_button = IconButton(
                parent=self,
                name='play',
                url='https://i.imgur.com/HAmphYK.png',
                command=self.play
            )
            self.elements.update(
                {
                    'play_button': {
                        'widget': self.play_button,
                        'color': SQUARE_BACKGROUND,
                        'active_color': ACTIVE_SQUARE_BACKGROUND
                    }
                }
            )
            if self.__update_available():
                image = 'https://i.imgur.com/82BLmgK.png'
                update_button_name = 'update_available'
            else:
                self.update_available = True
                update_button_name = 'update_not_available'
                image = 'https://i.imgur.com/P6KsRte.png'
            self.update_button = IconButton(
                parent=self, name=update_button_name, url=image,
                command=self.update
            )
            self.elements.update(
                {
                    'update_button': {
                        'widget': self.update_button,
                        'color': SQUARE_BACKGROUND,
                        'active_color': ACTIVE_SQUARE_BACKGROUND
                    }
                }
            )
            self.current_version_label.grid(row=1, column=0, columnspan=3)
            self.play_button.grid(row=2, column=0)
            self.update_button.grid(row=2, column=2)
            self.bind('<Enter>', self.__set_active)
            self.bind('<Leave>', self.__set_inactive)
            self.bind('<Button-1>', self.show_details)
        else:
            self.current_version_label = tkinter.Label(
                self, text='Not installed', background=SQUARE_BACKGROUND,
                foreground=TEXT_COLOR, font=VERSION_FONT
            )
            self.install_button = IconButton(
                parent=self,
                name='install',
                url='https://i.imgur.com/I1EXSpj.png',
                command=self.install
            )
            self.install_button.configure(
                activebackground=ACTIVE_SQUARE_BACKGROUND
            )
            self.elements.update(
                {
                    'install_button': {
                        'widget': self.install_button,
                        'color': SQUARE_BACKGROUND,
                        'active_color': ACTIVE_SQUARE_BACKGROUND
                    }
                }
            )
            self.current_version_label.grid(row=1, column=0, columnspan=3)
            self.install_button.grid(row=2, column=1)
        self.elements.update(
            {
                'current_version': {
                    'widget': self.current_version_label,
                    'color': SQUARE_BACKGROUND,
                    'active_color': ACTIVE_SQUARE_BACKGROUND
                }
            }
        )

    def __set_active(self, event) -> None:
        del event
        self.configure(background=ACTIVE_SQUARE_BACKGROUND)
        for element in self.elements.values():
            element['widget'].configure(background=element['active_color'])

    def __set_inactive(self, event) -> None:
        del event
        self.configure(background=SQUARE_BACKGROUND)
        for element in self.elements.values():
            element['widget'].configure(background=element['color'])

    def __highlight(self, element: str, state: str) -> None:
        assert element in self.elements
        assert state in ['normal', 'error']
        if state == 'normal':
            normal_color = SQUARE_BACKGROUND
            active_color = ACTIVE_SQUARE_BACKGROUND
        else:
            normal_color = ERROR_SQUARE_BACKGROUND
            active_color = ERROR_ACTIVE_SQUARE_BACKGROUND
        self.elements[element]['color'] = normal_color
        self.elements[element]['active_color'] = active_color
        self.elements[element]['widget'].configure(
            activebackground=active_color
        )

    def show_details(self, event=None) -> None:
        del event
        details = DetailedView(parent=self)
        details.place(
            x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT
        )
        details.tkraise()

    def update(self) -> None:
        git_remote = self.config['remote']
        git_branch = self.config['branch']
        local_path = pathlib.Path(self.config['local_path'])
        local_path.parent.mkdir(parents=True, exist_ok=True)
        subprocess.run(
            f'git pull {git_remote} {git_branch}', shell=True,
            check=True, cwd=local_path
        )
        if not install_from_current_directory(path=local_path):
            self.__highlight(element='update_button', state='error')
        else:
            self.__highlight(element='update_button', state='normal')
        # Redraw everything but the title
        for key, element in self.elements.items():
            if key != 'title':
                element['widget'].grid_forget()
        self.__build_quick_access_interface()

    def play(self, parameter: str = '') -> None:
        if parameter:
            parameter = f' {parameter}'
        else:
            try:
                parameter = f" {self.config['default_start_parameter']}"
            except KeyError:
                parameter = ''
        try:
            subprocess.run(
                f"{python_executable()} -m {self.config['pip_name']}{parameter}",
                check=True, shell=True
            )
            self.__highlight(element='play_button', state='normal')
        except subprocess.CalledProcessError:
            self.__highlight(element='play_button', state='error')

    def install(self) -> None:
        git_url = self.config['url']
        git_branch = self.config['branch']
        local_path = pathlib.Path(self.config['local_path'])
        local_path.parent.mkdir(parents=True, exist_ok=True)
        if not local_path.exists():
            subprocess.run(
                f'git clone --branch {git_branch} {git_url}',
                shell=True, check=True, cwd=local_path.parent
            )
        if not install_from_current_directory(path=local_path):
            self.__highlight(element='install_button', state='error')
        else:
            self.__highlight(element='install_button', state='normal')
        # Redraw everything but the title
        for key, element in self.elements.items():
            if key != 'title':
                element['widget'].grid_forget()
        self.__build_quick_access_interface()

    def __get_current_version(self) -> str:
        pip_name = self.config['pip_name']
        command = f'{python_executable()} -m pip show {pip_name}'
        if os.name != 'nt':
            command = shlex.split(command)
        try:
            result = subprocess.check_output(command, universal_newlines=True)
            return [
                pair.split(': ')[1] for pair in result.split('\n')
                if pair.startswith('Version:')
            ][0]
        except subprocess.CalledProcessError:
            return 'not found'

    def __update_available(self) -> bool:
        if self.config['type'] == 'git':
            branch = self.config['branch']
            remote = self.config['remote']
            local_path = pathlib.Path(self.config['local_path'])
            local_rev_parse_command = f'git rev-parse {branch}'
            remote_rev_parse_command = f'git rev-parse {remote}/{branch}'
            merge_base_command = f'git merge-base {remote}/{branch} {branch}'
            if os.name != 'nt':
                local_rev_parse_command = shlex.split(
                    local_rev_parse_command
                )
                remote_rev_parse_command = shlex.split(
                    remote_rev_parse_command
                )
                merge_base_command = shlex.split(
                    merge_base_command
                )
            local_result = subprocess.check_output(
                local_rev_parse_command, universal_newlines=True,
                cwd=local_path
            )
            remote_result = subprocess.check_output(
                remote_rev_parse_command, universal_newlines=True,
                cwd=local_path
            )
            merge_base = subprocess.check_output(
                merge_base_command, universal_newlines=True, cwd=local_path
            )
            if local_result == merge_base and remote_result != merge_base:
                return True
        elif self.config['type'] == 'pip':
            pip_name = self.config['pip_name']
            for outdated_app in self.parent.outdated_apps:
                if outdated_app.startswith(pip_name):
                    return True
        return False

    def __update_remote(self) -> None:
        if self.config['type'] == 'git':
            git_remote = self.config['remote']
            git_branch = self.config['branch']
            local_path = pathlib.Path(self.config['local_path'])
            subprocess.run(
                f'git fetch {git_remote} {git_branch}',
                shell=True, check=True, cwd=local_path
            )


def main():
    config_handler = ConfigHandler()
    interface = Interface(config_handler=config_handler)
    interface.root.mainloop()


if __name__ == '__main__':
    main()
