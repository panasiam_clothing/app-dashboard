updater_configuration_schema = {
    "title": "Panasiam App updater configuration schema",
    "type": "object",
    "required": [
        "install_location",
        "applications"
    ],
    "properties": {
        "install_location": {
            "title": "Path to install location",
            "type": "string"
        },
        "applications": {
            "title": "Applications",
            "type": "array",
            "items": {
                "title": "Items",
                "type": "object",
                "required": [
                    "name",
                    "pip_name",
                    "configuration_path"
                ],
                "properties": {
                    "name": {
                        "title": "Name",
                        "type": "string"
                    },
                    "type": {
                        "title": "Source type",
                        "type": "string",
                        "enum": ["git", "pip"]
                    },
                    "pip_name": {
                        "title": "Name of the application on PIP",
                        "type": "string"
                    },
                    "configuration_path": {
                        "title": "Path to the configuration file",
                        "type": "string"
                    },
                    "default_start_parameter": {
                        "title": "Set of parameters to be attached at "
                        "execution",
                        "type": "string"
                    },
                    "parameter_configuration": {
                        "title": "Configurable parameter list",
                        "description": "Set of configurable parameter for the "
                        "user, that can be set or modified in the detailed "
                        "view of the GUI.",
                        "type": "array",
                        "items": {
                            "title": "configurable parameter",
                            "type": "object",
                            "required": [
                                "display_name",
                                "parameter_name",
                                "type"
                            ],
                            "properties": {
                                "display_name": {
                                    "title": "GUI display name",
                                    "type": "string",
                                },
                                "parameter_name": {
                                    "title": "CLI parameter name",
                                    "type": "string",
                                },
                                "type": {
                                    "title": "Type of the parameter",
                                    "type": "string",
                                    "enum": [
                                        "path", "url", "text"
                                    ]
                                },
                            }
                        }
                    }
                }
            },
            "dependencies": {
                "type": {
                    "oneOf": [
                        {
                            "properties": {
                                "type": {
                                    "enum": ["git"]
                                },
                                "url": {
                                    "title": "Url",
                                    "type": "string"
                                },
                                "branch": {
                                    "title": "Branch",
                                    "type": "string",
                                    "default": "main"
                                },
                                "remote": {
                                    "title": "Remote",
                                    "type": "string",
                                    "default": "origin"
                                },
                                "local_path": {
                                    "title": "Local_path",
                                    "type": "string"
                                }
                            },
                            "required": [
                                "url", "branch", "remote", "local_path"
                            ]
                        },
                        {
                            "properties": {
                                "type": {
                                    "enum": ['pip']
                                }
                            }
                        }
                    ]
                }
            }
        }
    }
}
