from app_dashboard.__main__ import (
    get_parameter_from_string, replace_parameter_in_string
)


def describe_get_parameter_from_string() -> None:
    def with_no_parameter() -> None:
        expected = {}
        result = get_parameter_from_string(string='abc')
        assert result == expected

    def with_one_parameter() -> None:
        expected = {'abc': '123'}
        result = get_parameter_from_string(string='--abc 123')
        assert result == expected

    def with_two_parameter() -> None:
        expected = {'abc': '123', 'bcd': '345'}
        result = get_parameter_from_string(string='--abc 123 --bcd 345')
        assert result == expected

    def with_value_surrounded_by_semicolon() -> None:
        expected = {'abc': 'test 1', 'bcd': '345'}
        result = get_parameter_from_string(string='--abc \"test 1\" --bcd 345')
        assert result == expected

    def with_single_dash_option() -> None:
        expected = {'abc': '123', 'b': '345'}
        result = get_parameter_from_string(string='--abc 123 -b 345')
        assert result == expected

    def with_single_dash_option_first() -> None:
        expected = {'a': '123', 'bcd': '345'}
        result = get_parameter_from_string(string='-a 123 --bcd 345')
        assert result == expected

    def with_invalid_inbetween() -> None:
        expected = {'abc': '123', 'cde': '567'}
        result = get_parameter_from_string(
            string='--abc 123 bcd 345 --cde 567'
        )
        assert result == expected

    def with_dash_in_value() -> None:
        expected = {'abc': 'test-dash'}
        result = get_parameter_from_string(
            string='--abc test-dash'
        )
        assert result == expected

    def with_double_dash_in_value() -> None:
        expected = {'abc': 'test--dash'}
        result = get_parameter_from_string(
            string='--abc test--dash'
        )
        assert result == expected


def describe_replace_parameter_in_string() -> None:
    def with_no_match() -> None:
        parameter = 'abc'
        new_value = '234'
        string = '--bcd 123'
        expectation = ''
        result = replace_parameter_in_string(
            parameter=parameter, new_value=new_value, string=string
        )
        assert expectation == result

    def with_one_simple_match() -> None:
        parameter = 'abc'
        new_value = '234'
        string = '--abc 123'
        expectation = '--abc 234'
        result = replace_parameter_in_string(
            parameter=parameter, new_value=new_value, string=string
        )
        assert expectation == result

    def with_replace_value_with_space_with_value_without_space() -> None:
        parameter = 'abc'
        new_value = '234'
        string = '--abc \"234 123\"'
        expectation = '--abc 234'
        result = replace_parameter_in_string(
            parameter=parameter, new_value=new_value, string=string
        )
        assert expectation == result

    def with_replace_value_without_space_with_value_with_space() -> None:
        parameter = 'abc'
        new_value = '\"234 123\"'
        string = '--abc 234'
        expectation = '--abc \"234 123\"'
        result = replace_parameter_in_string(
            parameter=parameter, new_value=new_value, string=string
        )
        assert expectation == result

    def with_multiple_parameter() -> None:
        parameter = 'abc'
        new_value = '\"234 123\"'
        string = '--bcd 345 --abc 234'
        expectation = '--bcd 345 --abc \"234 123\"'
        result = replace_parameter_in_string(
            parameter=parameter, new_value=new_value, string=string
        )
        assert expectation == result

    def with_single_dash_option() -> None:
        parameter = 'a'
        new_value = '123'
        string = '-a 234'
        expectation = '-a 123'
        result = replace_parameter_in_string(
            parameter=parameter, new_value=new_value, string=string
        )
        assert expectation == result

    def with_multiple_dash_options() -> None:
        parameter = 'a'
        new_value = '123'
        string = '--bcd 345 -a 234'
        expectation = '--bcd 345 -a 123'
        result = replace_parameter_in_string(
            parameter=parameter, new_value=new_value, string=string
        )
        assert expectation == result

    def with_multiple_options_replace_first_before_value_with_space() -> None:
        parameter = 'abc'
        new_value = '123'
        string = '--abc 345 --bcd \"234 123\"'
        expectation = '--abc 123 --bcd \"234 123\"'
        result = replace_parameter_in_string(
            parameter=parameter, new_value=new_value, string=string
        )
        assert expectation == result
